﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinServDemonDFS01.DAO_Imp;
using WinServDemonDFS01.Entidad;

namespace WinServDemonDFS01.DAO
{
    public class DAO_Datafeed
    {
        DAO_Datafeed_Imp datafeed = new DAO_Datafeed_Imp();
        public int consultarTablaTemporal(string proc)
        {
            var result = datafeed.consultarTablaTemporal(proc);
            return result;
        }
        public void actualizaInfoJob(string accion, string destino, string proceso, string file)
        {
            datafeed.actualizaInfoJob(accion, destino, proceso, file);
        }
        public void truncateTablaTemporal(string proc)
        {
            datafeed.truncateTablaTemporal(proc);
        }
        public string consultarEstadoCarga(string nombrefile, string destino, string proceso)
        {
            var result = datafeed.consultarEstadoCarga(nombrefile, destino, proceso);
            return result;
        }

        public int insertDataLoadFile(string nombrefile, string estado, string destino, string proc, string descripcion)
        {
            var result = datafeed.insertDataLoadFile(nombrefile, estado, destino, proc, descripcion);
            return result;
        }

        public ResponseFile consultarEstadoFile(string proc)
        {
            ResponseFile ResponseFile = datafeed.consultarEstadoFile(proc);
            return ResponseFile;
        }

        public string validarDestinoEnProceso(string destino, string proc)
        {
            var result = datafeed.validarDestinoEnProceso(destino, proc);
            return result;
        }

        public string updateDataLoadFile(string nombrefile, string proceso, string accion)
        {
            var result = datafeed.updateDataLoadFile(nombrefile, proceso, accion);
            return result;
        }
        public string consultarArchivoEnCopia(string nombrefile)
        {
            var result = datafeed.consultarArchivoEnCopia(nombrefile);
            return result;
        }

        public int insertCopyFile(string nombrefile, string estado, string proceso)
        {
            var result = datafeed.insertCopyFile(nombrefile, estado, proceso);
            return result;
        }

        public int updateCopyFile(string nombrefile, string estado, string proceso)
        {
            var result = datafeed.updateCopyFile(nombrefile, estado, proceso);
            return result;
        }
    }
}