﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using WinServDemonDFS01.Util;

namespace WinServDemonDFS01
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new Service1() 
            };
            ServiceBase.Run(ServicesToRun);

            //Proceso proceso = new Proceso();
            //Ln_Global.WriteToFile("#--> Main " + DateTime.Now);
            //proceso.InicioProceso();
        }
    }
}
