﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using System.IO;
//Leer archivo
using System.Collections;
// Para añladir el tiempo
using System.Timers;

//Para comprimir y descomprimir archivos
using System.IO.Compression;

//para la conexion a la BBDD
using System.Runtime.Serialization;
using WinServDemonDFS01.Conexion;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
//using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Globalization;
using WinServDemonDFS01.Util;
using WinServDemonDFS01.DAO;
using WinServDemonDFS01.Entidad;

namespace WinServDemonDFS01
{
    public class Proceso
    {
        private static string pathRepo = @"C:\_DATAFEED\Repositorio\AmadeusIS\filesRepositorio";
        private static string pathDataFeed = @"C:\_DATAFEED\Repositorio\AmadeusIS\filesDatafeed\Proceso1";
        private static string pathDataFeed2 = @"C:\_DATAFEED\Repositorio\AmadeusIS\filesDatafeed\Proceso1\";
        private static string pathDataFeedMove = @"C:\_DATAFEED\Repositorio\AmadeusIS\filesDatafeedMove";
        private static string pathRepoCopy = @"C:\_DATAFEED\Repositorio\AmadeusIS\filesRepositorioProcesado";
        private static string strCredOraclBddev = "APPENGINE/appengine@10.75.102.16:1521/ORCL";

        private static string C_Proceso = "P1";
        private static string C_Accion = "INSERT";
        private static string C_Inactivo = "I";
        private static string C_Activo = "A";
        private static string C_Proc = "P";
        private static string C_EstadoCarga = "1";
        private static string C_EstadoFile_1 = "1";
        private static string C_EstadoFile_2 = "2";
        private static string C_EstadoFile_0 = "0";

        DAO_Datafeed datafeed = new DAO_Datafeed();

        public int InicioProceso()
        {
            ResponseFile rowsfile = new ResponseFile();

            int fileCount = Directory.GetFiles(pathRepo, "*.*").Length;
            //validamos si existe archivos en el repositorio
            if (fileCount > 0)
            {
                Ln_Global.WriteToFile("#####################################################################");
                Ln_Global.WriteToFile("########################INICIA PROCESADOR ###########################");
                Ln_Global.WriteToFile("#####################################################################");
                Ln_Global.WriteToFile("##---> INI StartProceso : " + DateTime.Now);

                Ln_Global.WriteToFile("PASO 1 consultarEstadoFile" + DateTime.Now);
                rowsfile = datafeed.consultarEstadoFile(C_Proceso);

                Ln_Global.WriteToFile("PASO 2 consultarEstadoCarga" + DateTime.Now);
                string estadoCargaJob = datafeed.consultarEstadoCarga(rowsfile.descripcion, rowsfile.destino, rowsfile.proceso);

                //Si estadoCargaJob = 1 (1=libre; 0 = EN EJECUCION)
                if (estadoCargaJob == C_EstadoCarga)
                {
                    //si sqllodar se esta ejecutando diferente de 2 (P = 2; 1 = A; 0 = libre)
                    if (rowsfile.codigo != C_EstadoFile_2)
                    {
                        Ln_Global.WriteToFile("PASO 3 updateDataLoadFile" + DateTime.Now);
                        datafeed.updateDataLoadFile(rowsfile.descripcion, C_Proceso, C_Inactivo);

                        if (rowsfile.codigo != C_EstadoFile_1)
                        {
                            C_EstadoFile_1 = "0";
                        }

                        Ln_Global.WriteToFile("PASO 4 updateCopyFile" + DateTime.Now);
                        string filegz = rowsfile.descripcion + ".gz";
                        datafeed.updateCopyFile(filegz, C_Inactivo, "");

                        //Eliminar el file procesado
                        if (File.Exists(pathDataFeed2 + rowsfile.descripcion))
                        {
                            Ln_Global.WriteToFile("PASO 5 eliminarOneFile : " + DateTime.Now);
                            eliminarOneFile(rowsfile.descripcion);
                        }
                        //Eliminar el file procesado amadeus
                        if (File.Exists(pathRepo + "\\" + rowsfile.descripcion))
                        {
                            Ln_Global.WriteToFile("PASO 6 eliminarOneFileAmadeus" + DateTime.Now);
                            eliminarOneFileAmadeus(rowsfile.descripcion);
                        }
                    }
                }

                //Si el estadoCargaJob esta libre (1=libre; 0 = EN EJCUCION) y archivo en proceso esta libre (1 = libre)
                if (estadoCargaJob == C_EstadoCarga && rowsfile.codigo == C_EstadoFile_1)
                {
                    //Truncate tabla temporal para procesar un nuevo archivo
                    Ln_Global.WriteToFile("PASO 7 truncateTablaTemporal" + DateTime.Now);
                    datafeed.truncateTablaTemporal(C_Proceso);

                    string fileName = "";
                    string sourcePath = pathRepo;
                    string targetPath = pathDataFeed;

                    string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                    string destFile = System.IO.Path.Combine(targetPath, fileName);

                    if (!System.IO.Directory.Exists(targetPath))
                    {
                        System.IO.Directory.CreateDirectory(targetPath);
                    }

                    if (System.IO.Directory.Exists(sourcePath))
                    {

                        string[] files = System.IO.Directory.GetFiles(sourcePath);

                        // Copy the files and overwrite destination files if they already exist.
                        foreach (string s in files)
                        {
                            // Use static Path methods to extract only the file name from the path.
                            fileName = System.IO.Path.GetFileName(s);
                            destFile = System.IO.Path.Combine(targetPath, fileName);
                            string fileEnCopia = "";
                            Ln_Global.WriteToFile("PASO 8 consultarArchivoEnCopia" + DateTime.Now);
                            Ln_Global.WriteToFile("PASO 9 FIle -> " + fileName);
                            fileEnCopia = datafeed.consultarArchivoEnCopia(fileName);

                            //Si el archivo esta tomado por otro proceso (0=SI; 1=NO)
                            if (fileEnCopia.Equals("1"))
                            {
                                Ln_Global.WriteToFile("PASO 10 insertCopyFile" + DateTime.Now);
                                datafeed.insertCopyFile(fileName, C_Activo, C_Proceso);
                                Ln_Global.WriteToFile("PASO 11 Copiar Archivo : " + DateTime.Now);
                                System.IO.File.Copy(s, destFile, true);
                                break;
                            }
                        }

                        //Logica para descomprimir archivos en gzip
                        DirectoryInfo directorySelected = new DirectoryInfo(targetPath);

                        foreach (FileInfo fileToDecompress in directorySelected.GetFiles("*.gz"))
                        {
                            string currentFileName2 = fileToDecompress.FullName;
                            string newFileName2 = currentFileName2.Remove(currentFileName2.Length - fileToDecompress.Extension.Length);
                            string file2 = newFileName2.Split('\\').Last();

                            //Descomprimir archivo
                            Ln_Global.WriteToFile("PASO 12 Decompress : " + DateTime.Now);
                            string rptaDesc = Decompress(fileToDecompress);

                            if (rptaDesc == "1")
                            {

                                //Leer la cantidad de lineas del archivo descomprimido
                                string currentFileName = fileToDecompress.FullName;
                                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                                int countLineas = 0;
                                int countLineas2 = 0;
                                int restarLineas = 36;
                                int restarLineas2 = 21;

                                try
                                {
                                    Ln_Global.WriteToFile("PASO 13 getTotalLineasV2 : " + DateTime.Now);
                                    countLineas = getTotalLineasV2(pathDataFeed2 + file2);

                                    if (countLineas > 0)
                                    {
                                        countLineas = countLineas - restarLineas;
                                        countLineas2 = countLineas - restarLineas2;
                                    }

                                    Ln_Global.WriteToFile("Total Lineas file txt : " + countLineas);
                                }
                                catch (Exception lin)
                                {
                                    Ln_Global.WriteToFile(" Error getTotalLineasV2: " + lin.Message);
                                }

                                //Como parametro enviamos el nombre del archivo desempaquetado
                                string file = newFileName.Split('\\').Last();
                                Ln_Global.WriteToFile("file : " + file);

                                if (file != "")
                                {

                                    // insertar tabla data file
                                    string estado = C_Activo;
                                    string descripcion = "";
                                    int insDLF = 0;
                                    try
                                    {
                                        Ln_Global.WriteToFile("PASO 14 getDestinoFile : " + DateTime.Now);
                                        string destination = getDestinoFile(pathDataFeed2 + file2);
                                        Ln_Global.WriteToFile("PASO 15 insertDataLoadFile : " + DateTime.Now);
                                        insDLF = datafeed.insertDataLoadFile(file, estado, destination, C_Proceso, descripcion);
                                    }
                                    catch (Exception ins)
                                    {
                                        Ln_Global.WriteToFile("Error insertDataLoadFile : " + ins);
                                    }

                                    if (insDLF > 0)
                                    {
                                        //Consultar cantidad de registros en tablas temporalesinsertadas
                                        int valCar = 0;
                                        try
                                        {
                                            Ln_Global.WriteToFile("PASO 16 consultarTablaTemporal : " + DateTime.Now);
                                            valCar = datafeed.consultarTablaTemporal(C_Proceso);
                                        }
                                        catch (Exception ex)
                                        {
                                            Ln_Global.WriteToFile("consultarTablaTemporal : " + ex.Message);
                                        }

                                        if (valCar < 1)
                                        {
                                            Ln_Global.WriteToFile("PASO 17 updateDataLoadFile : P " + DateTime.Now);
                                            datafeed.updateDataLoadFile(file, C_Proceso, C_Proc);
                                            Ln_Global.WriteToFile("PASO 18 EjecutarSqlloader : " + DateTime.Now);
                                            EjecutarSqlloader(file);
                                        }

                                        Ln_Global.WriteToFile("PASO 19 " + DateTime.Now);
                                        Ln_Global.WriteToFile("COMPARAR TXT(" + countLineas + ") = tabla temporal(" + valCar + ")");

                                        if (countLineas.Equals(valCar) || countLineas2.Equals(valCar))
                                        {
                                            Ln_Global.WriteToFile("PASO 20 getDestinoFile : " + DateTime.Now);
                                            string destination = getDestinoFile(pathDataFeed2 + file2);

                                            //Eliminar el file procesado
                                            Ln_Global.WriteToFile("PASO 21 eliminarOneFile : " + DateTime.Now);
                                            eliminarOneFile(file);

                                            //Eliminar el file procesado amadeus
                                            Ln_Global.WriteToFile("PASO 22 eliminarOneFileAmadeus" + DateTime.Now);
                                            eliminarOneFileAmadeus(file);

                                            //Se ejecuta el job
                                            try
                                            {
                                                Ln_Global.WriteToFile("PASO 23 updateDataLoadFile (A)" + DateTime.Now);
                                                datafeed.updateDataLoadFile(file, C_Proceso, C_Activo);
                                                Ln_Global.WriteToFile("PASO 24 actualizaInfoJob" + DateTime.Now);
                                                Ln_Global.WriteToFile("file : " + file);
                                                datafeed.actualizaInfoJob(C_Accion, destination, C_Proceso, file);
                                            }
                                            catch (Exception job)
                                            {
                                                Ln_Global.WriteToFile("Exception actualizaInfoJob..." + job.Message);
                                            }

                                        }
                                        else
                                        {
                                            Ln_Global.WriteToFile("PASO 25 BREAK : " + file);
                                            break;
                                        }
                                    }

                                }
                                else
                                {
                                    Ln_Global.WriteToFile("PASO 26 El archivo desempaquetado no existe... : " + DateTime.Now);
                                }

                            }


                        }
                    }
                    else
                    {
                        Ln_Global.WriteToFile("PASO 27 StartProceso - El path de origen no existe! : " + DateTime.Now);
                    }
                }
                //Si estado de carga no esta en ejecucion
                else if (estadoCargaJob == C_EstadoCarga)
                {

                    int countLineasTxt = 0;
                    int countLineasTxt2 = 0;
                    int restarLineas = 36;
                    string pathVal = "";
                    int restarLineas2 = 21;
                    try
                    {
                        Ln_Global.WriteToFile("PASO 28 getTotalLineasV2 : " + DateTime.Now);
                        Ln_Global.WriteToFile("Archivo file: " + rowsfile.descripcion);
                        pathVal = pathDataFeed2 + rowsfile.descripcion;
                        countLineasTxt = getTotalLineasV2(pathVal);

                        if (countLineasTxt > 0)
                        {
                            countLineasTxt = countLineasTxt - restarLineas;
                            countLineasTxt2 = countLineasTxt - restarLineas2;

                        }

                        Ln_Global.WriteToFile("Total Lineas file: " + countLineasTxt);
                    }
                    catch (Exception lin)
                    {
                        Ln_Global.WriteToFile("Error getTotalLineasV2 : " + lin.Message);
                    }

                    int countTablaTmp = 0;
                    try
                    {
                        Ln_Global.WriteToFile("PASO 29 consultarTablaTemporal" + DateTime.Now);
                        countTablaTmp = datafeed.consultarTablaTemporal(C_Proceso);
                        Ln_Global.WriteToFile("consultarTablaTemporal:" + countTablaTmp);
                    }
                    catch (Exception ex)
                    {
                        Ln_Global.WriteToFile("Error consultarTablaTemporal : " + ex.Message);
                    }

                    Ln_Global.WriteToFile("PASO 30 " + DateTime.Now);
                    Ln_Global.WriteToFile("COMPARAR TXT(" + countLineasTxt + ") = tabla temporal(" + countTablaTmp + ")");

                    if (countLineasTxt == 0 && countTablaTmp == 0)
                    {
                        datafeed.updateDataLoadFile("", C_Proceso, C_Activo);
                    }

                    if (countLineasTxt.Equals(countTablaTmp) || countLineasTxt2.Equals(countTablaTmp))
                    {
                        Ln_Global.WriteToFile("PASO 31 getDestinoFile : " + DateTime.Now);
                        string destination = getDestinoFile(pathVal);
                        //Eliminar el file procesado
                        Ln_Global.WriteToFile("PASO 32 eliminarOneFile : " + DateTime.Now);
                        eliminarOneFile(rowsfile.descripcion);

                        //Eliminar el file procesado amadeus
                        Ln_Global.WriteToFile("PASO 33 eliminarOneFileAmadeus" + DateTime.Now);
                        eliminarOneFileAmadeus(rowsfile.descripcion);

                        //Se ejecuta el job
                        try
                        {
                            Ln_Global.WriteToFile("PASO 34 updateDataLoadFile (A) : " + DateTime.Now);
                            datafeed.updateDataLoadFile(rowsfile.descripcion, C_Proceso, C_Activo);
                            Ln_Global.WriteToFile("PASO 35 actualizaInfoJob : " + DateTime.Now);
                            Ln_Global.WriteToFile("actualizaInfoJob: " + rowsfile.descripcion);
                            datafeed.actualizaInfoJob(C_Accion, destination, C_Proceso, rowsfile.descripcion);
                        }
                        catch (Exception job)
                        {
                            Ln_Global.WriteToFile("PASO 36 Exception actualizaInfoJob : " + job.Message);
                        }


                    }
                }
            }
            return 0;
        }
        /* Metodo que descomprime files
         * parametro, nombre del archivo a descomprimir
         * devuelve. es un void
         */
        public string Decompress(FileInfo fileToDecompress)
        {
            Ln_Global.WriteToFile("<INI Decompress>");
            Ln_Global.WriteToFile("fileToDecompress");

            string flag = "0";
            try
            {
                using (FileStream originalFileStream = fileToDecompress.OpenRead())
                {
                    string currentFileName = fileToDecompress.FullName;
                    string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                    flag = "1";
                    using (FileStream decompressedFileStream = File.Create(newFileName))
                    {
                        using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                        {
                            decompressionStream.CopyTo(decompressedFileStream);
                            Ln_Global.WriteToFile("Desempaquetado : " + fileToDecompress.Name);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Ln_Global.WriteToFile("##--->Exception Desempaquetado : " + e.Message + " <---##");

            }
            Ln_Global.WriteToFile("Flag : " + flag);
            Ln_Global.WriteToFile("<FIN Desempaquetad>");

            return flag;
        }

        /* Metodo que obtiene el total de lineas en un archivo de texto 
         * parametro, nombre del archivo extension .mr
         * devuelve el total de lineas
         */
        public int getTotalLineasV2(string newFileName)
        {
            Ln_Global.WriteToFile("<INI getTotalLineasV2>");
            Ln_Global.WriteToFile(newFileName);
            //string filename = @"\" + newFileName;
            int contador = 0;
            int intNoOfLines = 0;
            using (StreamReader oReader = new
            StreamReader(newFileName))
            {
                while (oReader.ReadLine() != null) intNoOfLines++;

                oReader.Close();
            }
            string[] strArrLines = new string[intNoOfLines];


            contador = strArrLines.Count();
            Ln_Global.WriteToFile("Valor : " + contador);
            Ln_Global.WriteToFile("<FIN getTotalLineasV2>");
            return contador;
        }

        public string getDestinoFile(string newFileName)
        {
            Ln_Global.WriteToFile("<INI getDestinoFile>");
            Ln_Global.WriteToFile(newFileName);

            int intNoOfLines = 0;
            string destino = "";
            using (StreamReader oReader = new
            StreamReader(newFileName))
            {
                string line = "";
                while ((line = oReader.ReadLine()) != null)
                {
                    intNoOfLines++;

                    if (intNoOfLines == 15)
                    {
                        string cadena = line.Replace("#", "").Replace("^", "");
                        string[] words = cadena.Split(':');
                        destino = words[1];
                        Ln_Global.WriteToFile(words[0] + " - " + words[1]);

                    }

                }

                oReader.Close();
            }
            Ln_Global.WriteToFile("Destino : " + destino);
            Ln_Global.WriteToFile("<FIN getDestinoFile : ");
            return destino;
        }
        public int getTotalLineas(string newFileName)
        {
            Ln_Global.WriteToFile("##--->INI getTotalLineas : " + DateTime.Now + " <---##");

            StreamReader objReader = new StreamReader(newFileName);
            string sLine = "";
            ArrayList arrText = new ArrayList();

            while (sLine != null)
            {
                sLine = objReader.ReadLine();
                if (sLine != null)
                    arrText.Add(sLine);
            }

            objReader.Close();
            Ln_Global.WriteToFile("##--->FIN getTotalLineas : " + DateTime.Now + " <---##");
            return arrText.Count;
        }
        /* Metodo para ejecutar procesos en segundo plano - SQLLOADER
         * parametro, nombre del archivo extension .mr
         * devuelve. es un void 
         */
        public string EjecutarSqlloader(string file)
        {
            Ln_Global.WriteToFile("<INI EjecutarSqlloader>");
            //string pathDataFeedLocal = @"\\nm49len\Todos\Gustavo Zuniga\filesDatafeed\";

            string valor = "";
            try
            {
                System.Diagnostics.Process process1;
                process1 = new System.Diagnostics.Process();
                process1.EnableRaisingEvents = false;

                //string strData = @" data=" + pathDataFeed + @"\" + file;
                string strSqlloader = @"/C SQLLDR ";
                string strDatosConex = strCredOraclBddev;
                string strControl = @" control=C:\_DATAFEED\ScriptCarga\AmadeusIS\DFS_CONTROL_P1.ctl";
                //string strData = @" data=C:\_DATAFEED\ScriptCarga\AmadeusIS\" + file;
                string strData = @" data=C:\_DATAFEED\Repositorio\AmadeusIS\filesDatafeed\Proceso1\" + file;
                string strLog = @" log=C:\_DATAFEED\Log\AmadeusIS\SqlLdr\DFS_LOG_P1_" + file + ".log";

                string strCmdLine;
                strCmdLine = strSqlloader + strDatosConex + strControl + strData + strLog;

                System.Diagnostics.Process.Start("CMD.exe", strCmdLine);
                process1.WaitForExit();

                process1.Close();


                if ((process1.ExitCode == 0))
                {
                    valor = "0";
                }
                else
                {
                    valor = process1.StandardError.ReadToEnd().ToString();
                }

                Ln_Global.WriteToFile("Se ejecuto - EjecutarSqlloader");
            }
            catch (Exception e)
            {
                Ln_Global.WriteToFile("##---> Exception EjecutarSqlloader ##" + e.Message + " - " + DateTime.Now + " <---##");
            }
            Ln_Global.WriteToFile("<FIN EjecutarSqlloader>");

            return valor;
        }
        public void Mover_Files()
        {
            Ln_Global.WriteToFile("##---> INI - Mover_Files ##" + DateTime.Now);
            string fileName = "";
            string sourcePath = pathDataFeed;
            string targetPath = pathRepoCopy;

            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copia los archivos y sobreescribe archivos del destino si ya existen.
                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(targetPath, fileName);
                    String extFile = System.IO.Path.GetExtension(fileName);
                    if (extFile.Equals(".gz"))
                    {
                        System.IO.File.Copy(s, destFile, true);
                    }

                }

            }
            else
            {
                Ln_Global.WriteToFile("##---> Mover_Files - El path de origen no existe ##" + DateTime.Now);
                Console.WriteLine("El path de origen no existe!");
            }
            Ln_Global.WriteToFile("##---> FIN - Mover_Files ##" + DateTime.Now);
        }

        public void Pasar_Files()
        {
            Ln_Global.WriteToFile("##---> INI - Pasar_Files ##" + DateTime.Now);
            string fileName = "";
            string sourcePath = pathDataFeed;
            string targetPath = pathDataFeedMove;

            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                // Copia los archivos y sobreescribe archivos del destino si ya existen.
                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(targetPath, fileName);
                    String extFile = System.IO.Path.GetExtension(fileName);
                    if (extFile.Equals(".gz"))
                    {
                        System.IO.File.Copy(s, destFile, true);
                    }

                }

            }
            else
            {
                Ln_Global.WriteToFile("##---> Pasar_Files - El path de origen no existe! ##" + DateTime.Now);
                Console.WriteLine("El path de origen no existe!");
            }
            Ln_Global.WriteToFile("##---> FIN - Pasar_Files ##" + DateTime.Now);
        }

        public void eliminarOneFile(string file)
        {
            Ln_Global.WriteToFile("##---> INI - eliminarOneFile ##" + DateTime.Now);

            try
            {
                String tempFolder = pathDataFeed;
                string fileName = "";
                string[] filePaths = Directory.GetFiles(tempFolder, "*.*");
                foreach (string filePath in filePaths)
                {
                    fileName = System.IO.Path.GetFileName(filePath);
                    if (fileName == (file + ".gz") || fileName == file)
                    {
                        File.Delete(filePath);
                        Ln_Global.WriteToFile(" Se elimina el archivo : " + file);
                    }

                }

            }
            catch (Exception ex)
            {
                Ln_Global.WriteToFile("##---> Error eliminarOneFile ## " + ex.ToString() + " - " + DateTime.Now);
                throw new Exception(ex.ToString());

            }
            Ln_Global.WriteToFile("##---> FIN - eliminarOneFile ##" + DateTime.Now);
        }

        public void eliminarOneFileAmadeus(string file)
        {
            Ln_Global.WriteToFile("##---> INI - eliminarOneFileAmadeus ##" + DateTime.Now);

            try
            {
                String tempFolder = pathRepo;
                string fileName = "";
                string[] filePaths = Directory.GetFiles(tempFolder, "*.*");
                foreach (string filePath in filePaths)
                {
                    fileName = System.IO.Path.GetFileName(filePath);
                    if (fileName == (file + ".gz"))
                    {
                        File.Delete(filePath);
                        Ln_Global.WriteToFile(" Se elimina el archivo : " + file);
                    }

                }

            }
            catch (Exception ex)
            {
                Ln_Global.WriteToFile("##---> Error eliminarOneFile ## " + ex.ToString() + " - " + DateTime.Now);
                throw new Exception(ex.ToString());

            }
            Ln_Global.WriteToFile("##---> FIN - eliminarOneFileAmadeus ##" + DateTime.Now);
        }
        public void Eliminar_Files()
        {
            Ln_Global.WriteToFile("##---> INI - Eliminar_Files ##" + DateTime.Now);

            try
            {
                String tempFolder = pathDataFeed;

                string[] filePaths = Directory.GetFiles(tempFolder);
                foreach (string filePath in filePaths)
                    File.Delete(filePath);
            }
            catch (Exception ex)
            {
                Ln_Global.WriteToFile("##---> Eliminar_Files ## " + ex.ToString() + " - " + DateTime.Now);
                throw new Exception(ex.ToString());

            }
            Ln_Global.WriteToFile("##---> FIN - Eliminar_Files ##" + DateTime.Now);
        }
        public void Eliminar_Files_Amadeus()
        {
            Ln_Global.WriteToFile("##---> INI - Eliminar_Files_Amadeus ##" + DateTime.Now);

            try
            {
                String tempFolder = pathRepo;

                string[] filePaths = Directory.GetFiles(tempFolder);
                foreach (string filePath in filePaths)
                    File.Delete(filePath);
            }
            catch (Exception ex)
            {
                Ln_Global.WriteToFile("##---> Eliminar_Files_Amadeus ##" + ex.ToString() + " - " + DateTime.Now);
                throw new Exception(ex.ToString());

            }
            Ln_Global.WriteToFile("##---> FIN - Eliminar_Files_Amadeus ##" + DateTime.Now);
        }

    }
}
