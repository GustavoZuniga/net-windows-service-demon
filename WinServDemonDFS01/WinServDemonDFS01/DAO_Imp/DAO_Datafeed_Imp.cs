﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinServDemonDFS01.Entidad;
using WinServDemonDFS01.Util;
using Oracle.DataAccess.Client;
using WinServDemonDFS01.Conexion;

namespace WinServDemonDFS01.DAO_Imp
{
    public class DAO_Datafeed_Imp
    {
        public int consultarTablaTemporal(string proc)
        {
            Ln_Global.WriteToFile("<INI consultarTablaTemporal>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_CONSULTAR_TABLETEMPORAL");
            int dblVentaTotal = 0;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_CONSULTAR_TABLETEMPORAL";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objCommand.Parameters.Add("pProceso", OracleDbType.Varchar2).Value = proc;
                        objCommand.Parameters.Add("ptotal", OracleDbType.Int32).Direction = ParameterDirection.Output;
                        
                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        dblVentaTotal = Convert.ToInt32(objCommand.Parameters["PTOTAL"].Value.ToString());

                    }
                }
            }
            catch (Exception ex)
            {

               //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : " + ex.Message);
                Ln_Global.WriteToFile("<FIN consultarTablaTemporal>");

            }
            Ln_Global.WriteToFile("Valor : " + dblVentaTotal);
            Ln_Global.WriteToFile("<FIN consultarTablaTemporal>");
            return dblVentaTotal;
        }

        public void truncateTablaTemporal(string proceso)
        {
            Ln_Global.WriteToFile("<INI truncateTablaTemporal>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_TRUNCATE_TABLETEMPORAL");
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_TRUNCATE_TABLETEMPORAL";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objCommand.Parameters.Add("pProceso", OracleDbType.Varchar2).Value = proceso;

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {

                //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : "+ex.Message);
                Ln_Global.WriteToFile("<FIN truncateTablaTemporal>");

            }
            Ln_Global.WriteToFile("<FIN truncateTablaTemporal>");

        }

        public void actualizaInfoJob(string accion, string destino, string proceso, string file)
        {
            Ln_Global.WriteToFile("<INI EJECUCION JOB>");
            Ln_Global.WriteToFile("PQ_DFS_PRC_DATAFILES.SP_LANZA_JOB_CARGA");
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_PRC_DATAFILES.SP_LANZA_JOB_CARGA";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objCommand.Parameters.Add("pAction", OracleDbType.Varchar2).Value = accion;
                        objCommand.Parameters.Add("pDestino", OracleDbType.Varchar2).Value = destino;
                        objCommand.Parameters.Add("pProceso", OracleDbType.Varchar2).Value = proceso;
                        objCommand.Parameters.Add("pFile", OracleDbType.Varchar2).Value = file;

                        //objCommand.Parameters.Add("@job_name", "INSERTAR_CONFIG");
                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {

                //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : " + ex.Message);
                Ln_Global.WriteToFile("<FIN EJECUCION JOB>");

            }
            Ln_Global.WriteToFile("<FIN EJECUCION JOB>");
        }
        public string consultarEstadoCarga(string nombrefile, string destino, string proceso)
        {
            Ln_Global.WriteToFile("<INI consultarEstadoCarga>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_ESTADO_CARGA");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            string codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_ESTADO_CARGA";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pFile", OracleDbType.Varchar2, nombrefile, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pDestino", OracleDbType.Varchar2, destino, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, proceso, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        codigo = objCommand.Parameters["pRptaCodigo"].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {

                //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : " + ex.Message);
                Ln_Global.WriteToFile("<FIN consultarEstadoCarga>");
                codigo = "error";

            }

            Ln_Global.WriteToFile("Estado de carga : " + codigo);
            Ln_Global.WriteToFile("<FIN consultarEstadoCarga>");
            return codigo;
        }

        public int insertDataLoadFile(string nombrefile, string estado, string destino, string proc, string descripcion)
        {
            Ln_Global.WriteToFile("<INI insertDataLoadFile>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.sp_insert_loadfile");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            int codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.sp_insert_loadfile";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pFile", OracleDbType.Varchar2, nombrefile, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pEstado", OracleDbType.Varchar2, estado, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pDestino", OracleDbType.Varchar2, destino, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, proc, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pDescripcion", OracleDbType.Varchar2, descripcion, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Int32, null, ParameterDirection.Output);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        codigo = Convert.ToInt32(objCommand.Parameters["pRptaCodigo"].Value.ToString());

                    }
                }
            }
            catch (Exception ex)
            {

                //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : " + ex.Message);
                Ln_Global.WriteToFile("<FIN insertDataLoadFile>");
                codigo = -1;

            }
            Ln_Global.WriteToFile("valor insert : "+codigo);
            Ln_Global.WriteToFile("<FIN insertDataLoadFile>");
            return codigo;
        }

        public ResponseFile consultarEstadoFile(string proc)
        {
            Ln_Global.WriteToFile("<INI consultarEstadoFile>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_CONSULTAR_ESTADO_FILE");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            ResponseFile rpta = new ResponseFile();

            //string codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_CONSULTAR_ESTADO_FILE";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pFile", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, proc, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaDestino", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaProceso", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaEstado", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        rpta.codigo = objCommand.Parameters["pRptaCodigo"].Value.ToString();
                        rpta.descripcion = objCommand.Parameters["pRptaMensaje"].Value.ToString();
                        rpta.destino = objCommand.Parameters["pRptaDestino"].Value.ToString();
                        rpta.proceso = objCommand.Parameters["pRptaProceso"].Value.ToString();
                        rpta.estado = objCommand.Parameters["pRptaEstado"].Value.ToString();
                        //codigo = objCommand.Parameters["pRptaCodigo"].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {

                //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : " + ex.Message);
                Ln_Global.WriteToFile("<FIN consultarEstadoFile>");
                rpta.codigo = "-1";

            }

            Ln_Global.WriteToFile("Valor : (" + rpta.codigo + ")");
            Ln_Global.WriteToFile("<FIN consultarEstadoFile>");
            return rpta;
        }
        public string validarDestinoEnProceso(string destino, string proc)
        {
            Ln_Global.WriteToFile("<INI validarDestinoEnProceso>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_VALIDAR_DESTINO_PROCESO");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            ResponseFile rpta = new ResponseFile();

            string codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_VALIDAR_DESTINO_PROCESO";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pDestino", OracleDbType.Varchar2, destino, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, proc, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        codigo = objCommand.Parameters["pRptaCodigo"].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.ToString());

            }

            Ln_Global.WriteToFile("Valor : "+codigo);
            Ln_Global.WriteToFile("<INI validarDestinoEnProceso>");
            return codigo;
        }
        public string updateDataLoadFile(string nombrefile, string proceso, string accion)
        {
            Ln_Global.WriteToFile("<INI consultarEstadoFile>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_UPDATE_ESTADO_FILE");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            ResponseFile rpta = new ResponseFile();

            string codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_UPDATE_ESTADO_FILE";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pFile", OracleDbType.Varchar2, nombrefile, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, proceso, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pAccion", OracleDbType.Varchar2, accion, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        codigo = objCommand.Parameters["pRptaCodigo"].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {

                //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : " + ex.Message);
                Ln_Global.WriteToFile("<FIN consultarEstadoFile>");
                codigo = "-1";

            }

            Ln_Global.WriteToFile("update file" + codigo);
            Ln_Global.WriteToFile("<FIN consultarEstadoFile>");
            return codigo;
        }
        public string consultarArchivoEnCopia(string archivo)
        {
            Ln_Global.WriteToFile("<INI consultarArchivoEnCopia>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_CONSULTAR_COPY_FILE");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            ResponseFile rpta = new ResponseFile();

            string codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_CONSULTAR_COPY_FILE";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pFile", OracleDbType.Varchar2, archivo, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, archivo, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        codigo = objCommand.Parameters["pRptaCodigo"].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {

                //throw new Exception(ex.ToString());
                Ln_Global.WriteToFile("Exception : " + ex.Message);
                Ln_Global.WriteToFile("<FIN consultarArchivoEnCopia>");
                codigo = "-1";


            }

            Ln_Global.WriteToFile("Valor : "+codigo);
            Ln_Global.WriteToFile("<INI consultarArchivoEnCopia>");
            return codigo;
        }

        public int insertCopyFile(string nombrefile, string estado, string proc)
        {
            Ln_Global.WriteToFile("<INI insertCopyFile>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_INSERT_COPYFILE");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            int codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_INSERT_COPYFILE";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pFile", OracleDbType.Varchar2, nombrefile, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pEstado", OracleDbType.Varchar2, estado, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, proc, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Int32, null, ParameterDirection.Output);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        codigo = Convert.ToInt32(objCommand.Parameters["pRptaCodigo"].Value.ToString());

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.ToString());

            }
            Ln_Global.WriteToFile("Valor : " + codigo);
            Ln_Global.WriteToFile("<FIN insertCopyFile>");
            return codigo;
        }
        public int updateCopyFile(string nombrefile, string estado, string proc)
        {
            Ln_Global.WriteToFile("<INI updateCopyFile>");
            Ln_Global.WriteToFile("PQ_DFS_BSQ_DATAFILES.SP_UPDATE_COPYFILE");

            NMOracleParameter objNMOracleParameter = new NMOracleParameter();

            int codigo;
            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_WebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_WebsOracle + ".PQ_DFS_BSQ_DATAFILES.SP_UPDATE_COPYFILE";
                        objCommand.CommandType = CommandType.StoredProcedure;

                        objNMOracleParameter.AddParameter(objCommand, "pFile", OracleDbType.Varchar2, nombrefile, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pEstado", OracleDbType.Varchar2, estado, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pProceso", OracleDbType.Varchar2, proc, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pRptaCodigo", OracleDbType.Int32, null, ParameterDirection.Output);
                        objNMOracleParameter.AddParameter(objCommand, "pRptaMensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.Connection = objConnection;

                        objCommand.ExecuteNonQuery();

                        codigo = Convert.ToInt32(objCommand.Parameters["pRptaCodigo"].Value.ToString());

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.ToString());

            }
            Ln_Global.WriteToFile("Valor : "+codigo);
            Ln_Global.WriteToFile("<FIN updateCopyFile>");
            return codigo;
        }
    }
}
