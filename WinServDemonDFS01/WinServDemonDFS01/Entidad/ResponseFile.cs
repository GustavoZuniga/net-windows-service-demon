﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinServDemonDFS01.Entidad
{
    public class ResponseFile
    {
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public string destino { get; set; }
        public string proceso { get; set; }
        public string estado { get; set; }
    }
}
